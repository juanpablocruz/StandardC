//
//  str.h
//  TestingC
//
//  Created by OrdenadorB on 24/01/14.
//  Copyright (c) 2014 pablo. All rights reserved.
//

#ifndef TestingC_str_h
#define TestingC_str_h

int _strlen(char *);
void _strcpy (char *, char *);
int _strend(char *, char*);
void _strcat(char *, char *);
int _strncmp (const char *, const char *, int);
char *_strncpy(char *, const char *, int );
char *_strncat (char *, const char*, int );
char *reverse(char *);

#endif
