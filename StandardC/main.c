//
//  main.c
//  TestingC/Users/ordenadorb/Desktop/graphics/ctest/ctest/main.c
//
//  Created by OrdenadorB on 21/01/14.
//  Copyright (c) 2014 pablo. All rights reserved.
//


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <math.h>
#include "_math.h"
#include "str.h"


#define SIZE 63

typedef struct {
    size_t alloc;
    int *storage;
} TEMP_STORAGE_T;

static inline int compute_minrun(int size) {
    int r = 0;  /* becomes 1 if the least significant bits contain at least one off bit */
    while (size >= 64) {
        r |= size & 1;
        size >>= 1;
    }
    return size + r;
}

void BinaryInsertionSort(int A[], size_t length) {
    if(length > 0) {
        int i,x,j;
        for (i = 1; i < length; i++) {
            x = A[i];
            j = i;
            while ( j > 0 && A[j-1] > x) {
                A[j] = A[j-1];
                j  = j - 1;
            }
            A[j] = x;
        }
    }
}

void tim_sort(int *dst, const int size) {
    if (size == 0) return;
    
    TEMP_STORAGE_T _store, *store;
    //int run_stack[128];
    int minrun;
    
    if (size < 64 ) {
        BinaryInsertionSort(dst, size);
        return;
    }
    
    minrun = compute_minrun(size);
    store = &_store;
    store->alloc = 0;
    store->storage = NULL;
    
}

static inline double utime() {
    struct timeval t;
    gettimeofday(&t, NULL);
    return (1000000.0 * t.tv_sec + t.tv_usec);
}




#define LOWER 0
#define UPPER 300
#define STEP 20

void fahrenheit() {
    float fahr, celsius;

    printf("\t  F\t   C\n");
    printf("\t-----------\n");
    
    for( fahr = UPPER; fahr != LOWER-STEP; fahr-= STEP) {
        celsius = (5.0/9.0) *  (fahr-32.0);
        printf("\t%3.0f\t%6.1f\n", fahr, celsius);

    }
}


void celsius() {
    float fahr, celsius;

    celsius = LOWER;
    printf("\t  C\t   F\n");
    printf("\t-----------\n");
    while (celsius <= UPPER) {
        fahr = ((9.0/5.0) * celsius) + 32;
        printf("\t%3.0f\t%6.1f\n",celsius, fahr);
        celsius = celsius + STEP;
    }
}

void checkEOF () {
    int c,blank = 0;
    
    while ((c = getchar()) != EOF) {
        switch (c) {
            case ' ':
                if( blank == 0) { printf(" "); blank++; }
                break;
            case '\t':
                printf("\\t");
                break;
            case '\b':
                printf("\\b");
                break;
            case '\\':
                printf("\\\\");
                break;
            default:
                blank = 0;
                printf ( "%c",c);
                break;
        }
    }
    
}

void histogram() {
    int c,tmp_len=0,i=0,max=0;
    int len[10];
    while ((c = getchar()) != EOF) {
        if((c=='\n' || c==' ') && i < 10){
            len[i] = tmp_len;
            i++;
            if (max < tmp_len)max = tmp_len;
            tmp_len = 0;
            if (c=='\n') {
                int t,k;
                for (k = max; k != 0; k--) {
                for (t = 0; t < i; t++) {
                    //for (k=0; k < len[t]; k++) printf("-");
                    //printf("\n");
                    if(len[t] >= k) printf("|");
                }
                    printf("\n");
                }
                for (t = 0; t < i; t++) {
                    for (k=0; k < len[t]; k++) printf("-");
                    printf("\n");
                }
            }
            
        } else {
            tmp_len++;
        }
    }
}


void charHistogram() {
    int c, i, max = 0;
    const int len = 122;
    int lista[len];
    for (i = 0; i < len; i++) {
        lista[i] = 0;
    }
    while ((c = getchar()) != EOF) {
        if (c=='\n') {
            int t,k;
            for (k = max; k != 0; k--) {
                for (t = 0; t < len; t++) {
                    //for (k=0; k < len[t]; k++) printf("-");
                    //printf("\n");
                    if(lista[t]!= 0 && lista[t] >= k) printf("|");
                }
                printf("\n");
            }
            for (t = 0; t < len; t++) {
                if(lista[t] != 0){
                    for (k=0; k < lista[t]; k++) printf("-");
                    printf("\n");
                }
            }
        } else {
            if (max < lista[c-1] + 1)max = lista[c-1] + 1;
            lista[c-1] = lista[c-1] + 1;
        }
    }
}


void reverseWord() {
    printf("%s\n",reverse("pepes"));
}


double compute_baby(double n, double current){
    return (1.0/2.0)*(current + (n/current));
}

double babylonian (double n) {
    double current = 100 * 100.0;
    int i;
    for (i = 0; i < 100; i++){
        current = compute_baby(n,current);
    }
    return current;
}


double elapsedTime(double (*f)(double)) {
    double t1,t2;
    t1 = utime();
    (*f)(125348);
    t2 = utime();
    return t2-t1;
}




int main(int argc, const char * argv[])
{
    
    int a[SIZE];
    int i;
    for ( i = 0; i < SIZE; i++) {
        a[i] = rand() % 50;
        printf("%d,",a[i]);
    }
    printf("\n\n");

    for ( i = 0; i < SIZE; i++) {
        printf("%d,",a[i]);
    }
    printf("\n\n");

   
    char *a1 = malloc(15);
    _strcpy(a1,"hola ");
    char *b = "caracola";
    reverseWord();
    printf("%s\n",_strncat(a1,b,3));
    printf("%u\n",_strncmp("hola","hola",3));
 
    //printf("root(125348)%f\n",_root(125348));
    //printf("_cos(60):%f\n",_cos(60*(PI/180)));
    //printf("cos(60):%f\n",cos(60*(PI/180)));
    printf("_sin(60):%f\n",_sin(60*(PI/180)));
    printf("_asin(_sin(60)): %f\n",_asin(_sin(60*(PI/180))));
    //printf("sin(60):%f\n",sin(60*(PI/180)));
    //printf("_tan(60):%f\n",_tan(60*(PI/180)));
    //printf("tan(60):%f\n",tan(60*(PI/180)));
    //printf("_ceil(%.1f):%d\n",2.5,_ceil(2.5));
    //printf("_floor(%.1f):%d\n",2.5,_floor(2.5));
    return 0;
}

