//
//  _math.h
//  TestingC
//
//  Created by OrdenadorB on 24/01/14.
//  Copyright (c) 2014 pablo. All rights reserved.
//

#ifndef TestingC__math_h
#define TestingC__math_h

#ifndef PI
#define PI 3.14159265358979323846
#endif

float _fabs( float );
static inline long long l2rbe(long long g, int e){
    long long A = 1;int i;
    for (i =  1<<((32 - __builtin_clz(e))-1); i!=0; i>>=1){
        A = A * A;
        if( (e & i )) A = A*g;
    }
    return A;
}
double _root(float);
double _cos (double );
double _sin(double);
double _tan (double);
double _asin (double a);
long long _fact (int);
int _ceil(double);
int _floor(double);
#endif
