//
//  _math.c
//  TestingC
//
//  Created by pablo on 24/01/14.
//  Copyright (c) 2014 pablo. All rights reserved.
//

#include "_math.h"
#include <math.h>
#include <stdio.h>

float _fabs( float f ) {
	int tmp = * ( int * ) &f;
	tmp &= 0x7FFFFFFF;
	return * ( float * ) &tmp;
}



double _root(float n) {
    float c = n;
    int i;
    for ( i = 100; i != 0; i--) c  = (c+(n/c))/2;
    return c;
}

long long _fact (int n) {
    int c;
    if(n==0)return 1;
    long long result = 1;
    for (c = 1; c <= n; c++)
        result = result * c;
    
    return result;
}

int _ceil(double a){
    return (int)a+1;
}
int _floor(double a){
    return ((int) a > a) ? (int) a-1: (int)a;
}
double _cos (double a) {
    double c = 0;
    int i;
    for ( i = 0; i < 30; i++) {
        c += l2rbe(-1,i)*((1.0/(_fact(2*i))) * powf(a,2*i));
    }
    return c;
}

double _sin(double a) {
    double c = 0;
    int i;
    for ( i = 0; i < 30; i++) {
        c += l2rbe(-1,i)*((1.0/(_fact((2*i)+1))) * powf(a,(2*i)+1));
    }
    return c;
}

double _tan (double a) {
    return _sin(a)/_cos(a);
}

double _fgamma ( n ) {
    return _fact(n-1);
}

double _asin (double a) {
    double c = 0;
    int i;
    for ( i = 0; i < 30; i++) {
        //c += l2rbe(-1,i)*((1.0/(_fact(2*i))) * powf(a,2*i));
        c += (_fgamma(i+1.0/2.0)/(_root(PI) * ((2*i) + 1) * _fact(i)))*powf(a, (2*i)+1) ;
        
    }
    return c * 180 / PI;
}


