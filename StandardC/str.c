//
//  str.c
//  TestingC
//
//  Created by OrdenadorB on 24/01/14.
//  Copyright (c) 2014 pablo. All rights reserved.
//

#include <stdlib.h>
#include "str.h"


int _strlen(char *s) { int n=0; while (*++s != '\0')n++;return n;}
void _strcpy (char *s, char *t) { while ((*s++ = *t++)); }
int _strcmp (char *s, char *t) {for(;*s == *t; s++, t++) { if (*s=='\0')return 0; } return *s - *t;}
void _strcat(char *s, char *t){ while (*++s!= '\0'){}; while ((*s++ = *t++));}

int _strend(char *s, char*t) {
    int len_t = _strlen(t);
    int len_s = _strlen(s);
    if ( len_t <= len_s) {
        s += len_s - len_t;
        if (0 == _strcmp(s,t))return 1;
    }
    return 0;
}

int _strncmp (const char *cs, const char *ct, int n) {
    while (n>0 && *cs == *ct && *cs != '\0'){
        ++cs;
        ++ct;
        --n;
    }
    if (n == 0 || *cs == *ct)return 0;
    if(*(unsigned char *) cs < *(unsigned char *) ct)return -1;
    return 1;
}
char *_strncpy(char *s, const char *ct, int n) {
    char *p;
    p = s;
    for(; n > 0 && *ct != '\0'; --n) *p++ = *ct++;
    for (; n > 0; --n)*p++ = '\0';
    return s;
}
char *_strncat (char *s, const char*ct, int n) {
    char *p;
    p = s;
    while (*++p != '\0');
    for(; n > 0 && *ct != '\0'; --n) *p++ = *ct++;
    *p = '\0';
    return s;
}
char* reverse(char* s) {
    char *tmp;
    int i=0,j;
    while(s[i] != '\0')i++;
    tmp = malloc(i+1);
    tmp[i] = '\0';
    for ( j = i-1; j != -1; j--)
        tmp[(i-1)-j] = s[j];
    return tmp;
}